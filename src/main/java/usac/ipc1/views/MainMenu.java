package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Juegos;
import usac.ipc1.dialogs.ConfigDialog;
import usac.ipc1.init.ReadFile;
import usac.ipc1.pdf.PdfGeneration;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;

public class MainMenu extends JFrame {
    public JPanel menuContainer, instructionsPanel, imagePanel;
    private JButton btnFile, btnPersonajes, btnNiveles, btnStart, btnConfig, btnPdf;
    private JLabel lblInstructions, lblImage, lblNombre;
    private JTextPane txaDescription;
    private JTextField txtNombre;

    public MainMenu() {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(700, 600));
        setPreferredSize(new Dimension(700, 600));
        setTitle("Río rapido");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildTopLayuot();
        buildRightLayuot();
        buildCenterLayout();


        pack();
        setVisible(true);
    }

    private void buildTopLayuot() {
        menuContainer = new JPanel();
        FlowLayout flowTop = new FlowLayout();
        flowTop.setAlignment(FlowLayout.LEFT);
        menuContainer.setLayout(flowTop);
        menuContainer.setBackground(Color.WHITE);
        menuContainer.setBorder(new MatteBorder(0, 0, 1, 0, Color.black));

        btnFile = new JButton();
        btnFile.setText("Archivo");
        btnFile.addActionListener(e -> openFile());

        btnNiveles = new JButton();
        btnNiveles.setText("Editar Niveles");
        btnNiveles.addActionListener(e -> {
            if (Principal.contadorNiveles > 0){
                dispose();
                new Tables(Principal.niveles, Table.TipoTabla.NIVEL);
            }else{
                JOptionPane.showMessageDialog(MainMenu.this,
                        "No se ha cargado ningun archivo .play",
                        "Configuracion",
                        JOptionPane.WARNING_MESSAGE);
            }

        });

        btnPersonajes = new JButton();
        btnPersonajes.setText("Editar Personajes");
        btnPersonajes.addActionListener(e -> {
            if (Principal.contadorNiveles > 0){
                dispose();
                new Tables(Principal.personajes, Table.TipoTabla.PERSONAJE);
            }else{
                JOptionPane.showMessageDialog(MainMenu.this,
                        "No se ha cargado ningun archivo .play",
                        "Configuracion",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        btnConfig = new JButton();
        btnConfig.setText("Configuracion");
        btnConfig.addActionListener(e -> {
            new ConfigDialog(MainMenu.this, false).setVisible(true);
        });

        btnPdf = new JButton();
        btnPdf.setText("Reporte juegos");
        btnPdf.addActionListener(e -> {
            if (Principal.contadorJuegos > 0){
                new PdfGeneration();
            }else{
                JOptionPane.showMessageDialog(MainMenu.this,
                        "No has jugado en ninguna ocasion",
                        "JUEGOS",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        menuContainer.add(btnFile);
        menuContainer.add(btnNiveles);
        menuContainer.add(btnPersonajes);
        menuContainer.add(btnConfig);
        menuContainer.add(btnPdf);

        add(menuContainer, BorderLayout.NORTH);
    }

    private void buildRightLayuot() {
        instructionsPanel = new JPanel();
        BoxLayout boxRight = new BoxLayout(instructionsPanel, BoxLayout.PAGE_AXIS);
        instructionsPanel.setLayout(boxRight);
        instructionsPanel.setBackground(Color.WHITE);
        instructionsPanel.setPreferredSize(new Dimension(300, getHeight()));
        instructionsPanel.setBorder(new MatteBorder(0, 1, 0, 0, Color.black));

        lblInstructions = new JLabel();
        lblInstructions.setAlignmentX(CENTER_ALIGNMENT);
        lblInstructions.setBorder(new EmptyBorder(10,0,10,0));
        lblInstructions.setText("Instrucciones");

        txaDescription = new JTextPane();
        txaDescription.setBorder(new EmptyBorder(10,10,10,0));
        txaDescription.setText("Deberas recordar al animal que veas en la primera ronda de cada nivel. \n" +
                "Con los controles indicados, \n" +
                "selecciona al animal que recuerdas. \n" +
                "Si el tiempo se acaba antes de que \n" +
                "llegues a la distancia maxima o \n" +
                "si el animal llega al final, \n " +
                "perderas. \n" +
                "El animal desaparecera luego de x\n" +
                "tiempo, lo cual dificultara el juego\n");
        txaDescription.setEnabled(false);
        txaDescription.setDisabledTextColor(Color.black);
        txaDescription.setPreferredSize(new Dimension(300,300));
        txaDescription.setMaximumSize(new Dimension(300,300));

        lblNombre = new JLabel("Ingresa tu nombre para jugar");

        txtNombre = new JTextField();
        txtNombre.setPreferredSize(new Dimension(getWidth(),25));
        txtNombre.setMaximumSize(new Dimension(getWidth(),25));
        txtNombre.setAlignmentX(CENTER_ALIGNMENT);

        btnStart = new JButton();
        btnStart.setText("Jugar");
        btnStart.addActionListener(e -> {
            if (txtNombre.getText().equals("")){
                JOptionPane.showMessageDialog(MainMenu.this,
                        "Ingrese un nombre para continuar",
                        "Campo vacio",
                        JOptionPane.WARNING_MESSAGE);
            }else{
                if (Principal.contadorNiveles > 0){
                    dispose();
                    Juegos juegos = new Juegos();
                    juegos.setId(Principal.contadorJuegos);
                    juegos.setNombreJugador(txtNombre.getText());
                    Principal.juegos[Principal.contadorJuegos] = juegos;
                    new Juego();
                }else{
                    JOptionPane.showMessageDialog(MainMenu.this,
                            "No se ha cargado ningun archivo .play",
                            "Configuracion",
                            JOptionPane.WARNING_MESSAGE);
                }
            }

        });

        instructionsPanel.add(lblInstructions);
        instructionsPanel.add(txaDescription);
        instructionsPanel.add(lblNombre);
        instructionsPanel.add(txtNombre);
        instructionsPanel.add(btnStart);

        add(instructionsPanel, BorderLayout.EAST);
    }

    private void openFile(){
        JFileChooser jFileChooser = new JFileChooser("../Desktop/");
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        FileFilter filtro = new FileNameExtensionFilter("Archivos PLAY (*.play)", "play");
        jFileChooser.setFileFilter(filtro);
        if (jFileChooser.showDialog(MainMenu.this, "Abrir archivo") == JFileChooser.APPROVE_OPTION){
            new ReadFile(jFileChooser.getSelectedFile(), MainMenu.this);
        }
    }

    private void buildCenterLayout(){
        imagePanel = new JPanel();
        imagePanel.setLayout(new OverlayLayout(imagePanel));
        imagePanel.setPreferredSize(new Dimension(400, getHeight()));

        lblImage = new JLabel(new ImageIcon(getClass().getResource("/waves2.png")));
        lblImage.setAlignmentX(CENTER_ALIGNMENT);

        JLabel lblTitle = new JLabel("PRACTICA 2");
        lblTitle.setFont(new Font("Arial", Font.PLAIN, 20));
        lblTitle.setVerticalAlignment(JLabel.CENTER);

        JPanel panel = new JPanel();
        FlowLayout flowCenter = new FlowLayout();
        panel.setLayout(flowCenter);
        panel.setPreferredSize(new Dimension(400, getHeight()));
        panel.setOpaque(false);
        panel.add(lblTitle);

        imagePanel.add(panel);
        imagePanel.add(lblImage);
        add(imagePanel, BorderLayout.CENTER);


    }
}
