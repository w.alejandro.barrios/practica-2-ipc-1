package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.dialogs.NivelDialog;
import usac.ipc1.dialogs.PersonajeDialog;

import javax.swing.*;
import java.awt.*;

public class Tables extends JFrame {

    private JPanel principalContainer;
    private JButton btnEdit;

    public Tables(Object vector[], Table.TipoTabla tipoTabla) {
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(800, 500));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Editar");

        panelPrincipal(vector, tipoTabla);

        setVisible(true);
    }

    private void panelPrincipal(Object vector[], Table.TipoTabla tipoTabla) {
        Table tabla = new Table(vector, tipoTabla);
        JScrollPane scrollPane = new JScrollPane(tabla);
        scrollPane.setMaximumSize(new Dimension(1500, 184));

        principalContainer = new JPanel();
        principalContainer.setSize(500, 200);
        principalContainer.setBackground(Color.white);

        GroupLayout groupLayout = new GroupLayout(principalContainer);
        groupLayout.setAutoCreateGaps(true);
        groupLayout.setAutoCreateContainerGaps(true);
        principalContainer.setLayout(groupLayout);

        JButton btnBack = new JButton("Volver");
        btnBack.setFocusPainted(false);
        btnBack.addActionListener(e -> {
            dispose();
            new MainMenu();
        });

        btnEdit = new JButton("Editar");
        btnEdit.setFocusPainted(false);
        btnEdit.addActionListener(e -> {
            if (tabla.getSelectedRowCount() >0){
                switch (tipoTabla){
                    case NIVEL:
                        new NivelDialog(Tables.this, false, Principal.niveles, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;

                    case PERSONAJE:
                        new PersonajeDialog(Tables.this, false, Principal.personajes, tabla, tabla.getSelectedRow()).setVisible(true);
                        break;
                }
            }else{
                JOptionPane.showMessageDialog(Tables.this,
                        "Debe seleccionar la fila a editar",
                        "Editar fila",
                        JOptionPane.WARNING_MESSAGE);
            }
        });

        groupLayout.setHorizontalGroup(
                groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, true)
                        .addComponent(btnBack)
                        .addComponent(scrollPane)
                        .addComponent(btnEdit)
        );
        groupLayout.setVerticalGroup(
                groupLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addComponent(scrollPane)
                        .addComponent(btnEdit)
        );

        add(principalContainer, BorderLayout.CENTER);
        revalidate();
        repaint();

    }

}
