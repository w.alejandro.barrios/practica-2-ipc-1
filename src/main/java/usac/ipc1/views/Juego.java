package usac.ipc1.views;

import usac.ipc1.Principal;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Juego extends JFrame {

    private JPanel informationPanel, gamePanel, carrierPanel, carrierLeft, carrierRight;
    public JLabel lblLevel, lblTiempoRestante, lblPoints, lblMaxDistance, lblVelocity, lblControls, lblPausar, lblFinalizar, lblRecorrida;
    private JLabel lblBackground;
    private Jugar jugar;

    public Juego(){
        super();
        setLayout(new BorderLayout());
        setBounds(new Rectangle(700, 800));
        setPreferredSize(new Dimension(700, 800));
        setTitle("Río rapido");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        buildRightLayout();
        buildLeftLayout();


        pack();
        setVisible(true);
    }

    private void buildRightLayout(){
        informationPanel = new JPanel();
        BoxLayout boxLayout = new BoxLayout(informationPanel, BoxLayout.Y_AXIS);
        informationPanel.setLayout(boxLayout);
        informationPanel.setBackground(Color.WHITE);
        informationPanel.setPreferredSize(new Dimension(300, getHeight()));
        informationPanel.setBorder(new MatteBorder(0, 1, 0, 0, Color.black));

        lblLevel = new JLabel();
        lblLevel = buildLabel(lblLevel, "NIVEL: ");

        lblTiempoRestante = new JLabel();
        lblTiempoRestante = buildLabel(lblTiempoRestante, "TIEMPO RESTANTE: ");

        lblPoints = new JLabel();
        lblPoints = buildLabel(lblPoints, "PUNTOS: ");

        lblMaxDistance = new JLabel();
        lblMaxDistance = buildLabel(lblMaxDistance, "DISNTACIA MÁXIMA: ");

        lblRecorrida = new JLabel();
        lblRecorrida = buildLabel(lblRecorrida, "DISTANCIA RECORRIDA: 0");

        lblVelocity = new JLabel();
        lblVelocity = buildLabel(lblVelocity, "VELOCIDAD: ");

        lblControls = new JLabel();
        lblControls = buildLabel(lblControls, "CONTROLES: ");

        lblPausar = new JLabel();
        lblPausar = buildLabel(lblPausar, "Para pausar presione la tecla '" + Principal.pause + "'");

        lblFinalizar = new JLabel();
        lblFinalizar = buildLabel(lblFinalizar, "Para finalizar presinona la tecla '" + Principal.finish + "'");


        informationPanel.add(Box.createVerticalGlue());
        informationPanel.add(lblLevel);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblTiempoRestante);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblPoints);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblMaxDistance);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblRecorrida);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblVelocity);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblControls);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblPausar);
        informationPanel.add(Box.createRigidArea(new Dimension(0, 1)));
        informationPanel.add(lblFinalizar);
        informationPanel.add(Box.createVerticalGlue());

        add(informationPanel, BorderLayout.EAST);
    }

    private void buildLeftLayout(){
        gamePanel = new JPanel();
        gamePanel.setLayout(new OverlayLayout(gamePanel));
        gamePanel.setPreferredSize(new Dimension(400, getHeight()));

        lblBackground = new JLabel(new ImageIcon(getClass().getResource("/waves2.png")));
        lblBackground.setAlignmentX(CENTER_ALIGNMENT);

        jugar = new Jugar(this);

        carrierPanel = new JPanel(new BorderLayout());
        carrierPanel.setPreferredSize(new Dimension(400, getHeight()));
        carrierPanel.setOpaque(false);

        carrierLeft = new JPanel();
        carrierLeft.setLayout(new FlowLayout());
        carrierLeft.setPreferredSize(new Dimension(200, getHeight()));
        carrierLeft.setOpaque(false);

        carrierRight = new JPanel();
        carrierRight.setLayout(new FlowLayout());
        carrierRight.setPreferredSize(new Dimension(200, getHeight()));
        carrierRight.setOpaque(false);

        carrierPanel.add(carrierLeft, BorderLayout.WEST);
        carrierPanel.add(carrierRight, BorderLayout.EAST);

        gamePanel.add(jugar);
        gamePanel.add(lblBackground);

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                jugar.keyPressed(e);
            }
        });

        add(gamePanel, BorderLayout.CENTER);
    }

    private JLabel buildLabel(JLabel label, String name){
        label.setAlignmentX(CENTER_ALIGNMENT);
        label.setBorder(new EmptyBorder(10,0,10,0));
        label.setText(name);
        return label;
    }

}
