package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Nivel;
import usac.ipc1.beans.Personaje;
import usac.ipc1.models.TablaNiveles;
import usac.ipc1.models.TablaPersonajes;

import javax.swing.*;

public class Table extends JTable {

    private Nivel vectorNivel[];
    private Personaje vectorPersonaje[];
    public enum TipoTabla {NIVEL, PERSONAJE}
    private TipoTabla tipoTabla;

    public Table(){
        super();
    }

    public Table(Object vector[], TipoTabla tipoTabla){
        super();
        switch (tipoTabla){
            case NIVEL:
                this.vectorNivel = (Nivel[]) vector;
                setModel(new TablaNiveles(vectorNivel));
                break;

            case PERSONAJE:
                this.vectorPersonaje = (Personaje[]) vector;
                setModel(new TablaPersonajes(vectorPersonaje));
                break;
        }
    }

    //region  nivel
    public void editar(Nivel nivel, int position){
        vectorNivel[position] = nivel;
        Principal.niveles[position] = nivel;
        revalidate();
        repaint();
    }
    //endregion

    //region Personaje
    public void editar(Personaje personaje, int position){
        vectorPersonaje[position] = personaje;
        Principal.personajes[position] = personaje;
        revalidate();
        repaint();
    }
    //endregion

}
