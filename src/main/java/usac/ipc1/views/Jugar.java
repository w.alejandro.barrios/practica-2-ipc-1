package usac.ipc1.views;

import usac.ipc1.Principal;
import usac.ipc1.beans.Personaje;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Jugar extends JPanel implements Runnable {

    private Personaje animal1, animal2;
    private final int ALTO_ESCENARIO = 800;
    private final int ANCHO_ESCENARIO = 400;
    private final int ALTO_PERSONAJE = 25;
    private int desplazamientoY1, desplazamientoY2, puntos;
    private boolean pausado, jugando, isInvisible, checked, finalizado;
    private Juego juego;
    private long tiempo;
    private Thread hilo;
    private final Object clave = new Object();
    private int selectionSide, selectioned, ronda, lastSelection, nivelActual, desplazar1, desplazar2, time, distanciaAcumulada;
    private Timer timerInvisible, timer;

    public Jugar(Juego juego) {
        super();
        this.juego = juego;
        setLayout(null);
        setBounds(0, 0, ANCHO_ESCENARIO, ALTO_ESCENARIO);
        setOpaque(false);
        nivelActual = 1;
        puntos = 0;
        distanciaAcumulada = 0;
        finalizado = false;
        isInvisible = false;
        checked = false;
        start();
        iniciarJuego();
    }

    private void start() {
        iniciar();
        if (nivelActual != 1)
            jugando = true;
        printCharacter();
        time = Principal.niveles[nivelActual - 1].getTiempoNivel();
        juego.lblTiempoRestante.setText("Tiempo restante: " + Principal.niveles[nivelActual - 1].getTiempoNivel() + "s");
        //System.out.println("NIVELES " + Principal.contadorNiveles);
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                restarTiempo();
                juego.lblTiempoRestante.setText("Tiempo restante: " + time + "s");
                if (time == 0) {
                    pausar();
                    timer.cancel();
                    timer.purge();
                    if (distanciaAcumulada < Principal.niveles[nivelActual - 1].getDistanciaMaxima()){
                        if (!finalizado)
                            finalizar();
                    }
                    else {
                        if (nivelActual < Principal.contadorNiveles) {
                            timerInvisible.cancel();
                            timerInvisible.purge();
                            nivelActual++;
                            start();
                        }
                    }

                }
            }
        }, 0, 1000);
    }

    private void restarTiempo() {
        if (!pausado)
            time--;
    }

    private void iniciar() {
        ronda = 1;
        jugando = false;
        pausado = false;
        if (nivelActual == 1) {
            desplazamientoY1 = 800 / Principal.niveles[0].getDistanciaPantalla();
            desplazamientoY2 = 800 / Principal.niveles[0].getDistanciaPantalla();
        } else {
            desplazamientoY1 = 800 / (Principal.niveles[Principal.contadorNiveles - 1].getDistanciaPantalla() - Principal.niveles[nivelActual - 2].getDistanciaPantalla());
            desplazamientoY2 = 800 / (Principal.niveles[Principal.contadorNiveles - 1].getDistanciaPantalla() - Principal.niveles[nivelActual - 2].getDistanciaPantalla());
        }
        desplazar1 = desplazamientoY1;
        desplazar2 = desplazamientoY2;
        tiempo = 100 - Principal.niveles[nivelActual - 1].getVelocidadCorriente();
        juego.lblLevel.setText("NIVEL: " + Principal.niveles[nivelActual - 1].getNivelId());
        juego.lblPoints.setText("PUNTOS: " + puntos);
        juego.lblMaxDistance.setText("DISTANCIA MAX: " + Principal.niveles[nivelActual - 1].getDistanciaMaxima());
        juego.lblVelocity.setText("VELOCIDAD: " + Principal.niveles[nivelActual - 1].getVelocidadCorriente() + "m/s");
        juego.lblControls.setText("CONTROLES: " + Principal.selectLeft +" | " + Principal.selectRight);

    }

    private void printCharacter() {
        //System.out.println("RONDA " + ronda);
        if (animal1 != null) {
            remove(animal1);
            animal1.setBounds(100, 0, 25, 25);
        }

        if (animal2 != null) {
            remove(animal2);
            animal2.setBounds(100, 0, 25, 25);
        }
        int resultado;
        int low = 0;
        int high = Principal.contadorPersonajes - 1;
        //System.out.println("PERSONAJES: " + Principal.contadorPersonajes);
        int result = 0;
        if (ronda == 1) {
            Random r = new Random();
            result = r.nextInt(high - low) + low;
            animal1 = Principal.personajes[result];
            add(animal1);
            selectionSide = 1;
            lastSelection = result;
        }

        if (ronda != 1) {
            if (selectioned == 1) {
                do {
                    Random r = new Random();
                    result = r.nextInt(high - low) + low;
                } while (result == lastSelection);
                animal1 = Principal.personajes[result];
            } else {
                animal1 = Principal.personajes[lastSelection];
                selectionSide = 1;
            }

            add(animal1);
            if (selectioned == 2) {
                do {
                    Random r2 = new Random();
                    resultado = r2.nextInt(high - low) + low;
                } while (resultado == lastSelection);
                animal2 = Principal.personajes[resultado];
            } else {
                animal2 = Principal.personajes[lastSelection];
                selectionSide = 2;
            }

            animal2.setBounds(animal2.getLocation().x + 200, animal2.getLocation().y, ALTO_PERSONAJE, ALTO_PERSONAJE);
            add(animal2);
            //System.out.println("ANIMAL 1: " + animal1.getNombre());
            //System.out.println("ANIMAL 2: " + animal2.getNombre());
        }
        revalidate();
        repaint();
    }

    void actualizarPosicion() {
        int nuevaUbicacion2 = 0;
        int nuevaUbicacion1 = animal1.getLocation().y + desplazamientoY1;
        if (animal2 != null)
            nuevaUbicacion2 = animal2.getLocation().y + desplazamientoY2;
        //System.out.println(nuevaUbicacion1);

        if (nuevaUbicacion1 > ALTO_ESCENARIO - ALTO_PERSONAJE) {
            finalizar();
        } else if (nuevaUbicacion1 <= 0) {
            animal1.setVisible(true);
            if (animal2 != null)
                animal2.setVisible(true);

            if (!checked) {
                //System.out.println("CAMBIO DE UBICACION");
                if (isInvisible)
                    makeInvisible();
                pausar();
                desplazamientoY1 = desplazar1;
                desplazamientoY2 = desplazar2;
                printCharacter();
                reanudar();
                timerInvisible = new Timer();
                timerInvisible.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        makeInvisible();
                        timerInvisible.cancel();
                        timerInvisible.purge();
                    }
                }, Principal.niveles[nivelActual - 1].getTiempoSumergir() * 1000);
                checked = true;
            }

        }

        animal1.setBounds(animal1.getLocation().x, nuevaUbicacion1, ALTO_PERSONAJE, ALTO_PERSONAJE);
        if (animal2 != null)
            animal2.setBounds(animal2.getLocation().x, nuevaUbicacion2, ALTO_PERSONAJE, ALTO_PERSONAJE);
        revalidate();
        repaint();
    }

    public void iniciarJuego() {
        if (hilo == null) {
            jugando = true;
            hilo = new Thread(this, "Juego");
            hilo.start();
        }
    }

    public void pausar() {
        pausado = true;
    }

    public void pausarKey() {
        pausado = true;
        juego.lblPausar.setText("Para reanudar presione la tecla '" + Principal.playAgain + "'");

    }

    public void reanudarKey(){
        juego.lblPausar.setText("Para pausar presione la tecla '" +  Principal.pause + "'");
        reanudar();
    }

    private void makeInvisible() {
        if (isInvisible) {
            animal1.setVisible(true);
            animal2.setVisible(true);
            isInvisible = false;
        } else {
            animal1.setVisible(false);
            animal2.setVisible(false);
            isInvisible = true;
        }

    }

    synchronized void reanudar() {
        synchronized (clave) {
            pausado = false;
            clave.notifyAll();
        }
    }

    @Override
    public void run() {
        while (jugando) {
            synchronized (clave) {
                if (!pausado) {
                    actualizarPosicion();
                    try {
                        Thread.sleep(tiempo);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        clave.wait();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Jugar.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

    }

    public void keyPressed(KeyEvent e) {
        // IZQUIERDA
        if (e.getKeyCode() == Principal.selectLeftCode) {
            selectioned = 1;
            //System.out.println("IZQUIERDA");
            if (selectioned == selectionSide) {
                desplazamientoY1 = -ALTO_PERSONAJE;
                desplazamientoY2 = -ALTO_PERSONAJE;
                if (isInvisible)
                    puntos += Principal.niveles[nivelActual - 1].getPuntosSeleccionSumergir();
                else
                    puntos += Principal.niveles[nivelActual - 1].getPuntosSeleccion();
                if (!isInvisible && timerInvisible != null) {
                    timerInvisible.cancel();
                    timerInvisible.purge();
                }
                if (ronda != 1) {
                    if (isInvisible)
                        makeInvisible();
                }
                distanciaAcumulada += animal1.getLocation().y;
                if (distanciaAcumulada >= Principal.niveles[nivelActual - 1].getDistanciaMaxima())
                    nextLevel();
                ronda++;
                checked = false;
            }
        }

        // DERECHA
        if (e.getKeyCode() == Principal.selectRightCode) {
            selectioned = 2;
            if (selectioned == selectionSide) {
                desplazamientoY1 = -ALTO_PERSONAJE;
                desplazamientoY2 = -ALTO_PERSONAJE;
                if (isInvisible)
                    puntos += Principal.niveles[nivelActual - 1].getPuntosSeleccionSumergir();
                else
                    puntos += Principal.niveles[nivelActual - 1].getPuntosSeleccion();
                if (!isInvisible && timerInvisible != null) {
                    timerInvisible.cancel();
                    timerInvisible.purge();
                }
                if (ronda != 1) {
                    if (isInvisible)
                        makeInvisible();
                }
                distanciaAcumulada += animal2.getLocation().y;
                if (distanciaAcumulada >= Principal.niveles[nivelActual - 1].getDistanciaMaxima())
                    nextLevel();
                ronda++;
                checked = false;
            }
        }

        // PAUSAR
        if (e.getKeyCode() == Principal.pauseCode) {
            pausarKey();
        }

        // REANUDAR
        if (e.getKeyCode() == Principal.playAgainCode){
            reanudarKey();
        }

        // FINALIZAR
        if (e.getKeyCode() == Principal.finishCode) {
            finalizar();
        }

        juego.lblPoints.setText("PUNTOS: " + puntos);
        juego.lblRecorrida.setText("DISTANCIA RECORRIDA: " + distanciaAcumulada);
    }

    private void nextLevel() {

        if (nivelActual < Principal.contadorNiveles) {
            timerInvisible.cancel();
            timerInvisible.purge();
            nivelActual++;
            start();
        }else{
            finalizar();
        }

    }

    private void finalizar() {
        finalizado = true;
        pausar();
        Principal.juegos[Principal.contadorJuegos].setPuntosObtenidos(puntos);
        JOptionPane.showMessageDialog(Jugar.this,
                Principal.juegos[Principal.contadorJuegos].getNombreJugador() + " Tienes una puntuacion de " + Principal.juegos[Principal.contadorJuegos].getPuntosObtenidos(),
                "PUNTUACION FINAL",
                JOptionPane.INFORMATION_MESSAGE);
        Principal.contadorJuegos++;
        this.hilo = null;
        juego.dispose();
        new MainMenu();
    }

}
