package usac.ipc1.pdf;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import usac.ipc1.Principal;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PdfGeneration {

    public static final String DEST = "results/chapter01/Report.pdf";

    public PdfGeneration() {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        try {
            createPdf(DEST);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void createPdf(String destination) throws IOException {
        // Initialize PDF
        FileOutputStream fileOutputStream = new FileOutputStream(destination);
        PdfWriter writer = new PdfWriter(fileOutputStream);

        // Initialize pdf document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf);

        // Content
        float[] pointColumnWidths = {100F, 100F};
        Table table = new Table(pointColumnWidths);
        Cell cell1 = new Cell();
        Cell cell2 = new Cell();

        document.add(new Paragraph("LISTADO DE JUEGOS"));
        document.add(new Paragraph(""));

        cell1.add("Jugador");
        cell2.add("Puntos");
        int counterEmpty = 0;
        for (int i = 0; i < Principal.juegos.length;i++){
            if (Principal.juegos[i] != null)
                counterEmpty++;
        }
        int[][] juegosOrder = new int[counterEmpty][2];
        for (int contador = 0; contador < counterEmpty; contador++) {
            if (Principal.juegos[contador] != null) {
                juegosOrder[contador][0] = Principal.juegos[contador].getId();
                juegosOrder[contador][1] = Principal.juegos[contador].getPuntosObtenidos();
            }

        }

        for (int i = 0; i < juegosOrder.length - 1; i++) {
            for (int a = 0; a < juegosOrder.length - i - 1; a++) {
                if (juegosOrder[a][1] < juegosOrder[a + 1][1]) {
                    int temporal = juegosOrder[a + 1][1];
                    int idJuego = juegosOrder[a + 1][0];
                    juegosOrder[a + 1][1] = juegosOrder[a][1];
                    juegosOrder[a + 1][0] = juegosOrder[a][0];
                    juegosOrder[a][1] = temporal;
                    juegosOrder[a][0] = idJuego;
                }
            }
        }

        for (int i = 0; i < juegosOrder.length; i++) {
            if (juegosOrder[i] != null){
                for (int o = 0; o < Principal.juegos.length; o++) {
                    if (Principal.juegos[o] != null) {
                        if (Principal.juegos[o].getId() == juegosOrder[i][0]) {
                            cell1.add(Principal.juegos[o].getNombreJugador());
                            cell2.add(Integer.toString(juegosOrder[i][1]));
                        }

                    }
                }
            }
        }

        table.addCell(cell1);
        table.addCell(cell2);

        document.add(table);

        // Close document
        document.close();

        File myFile = new File(destination);
        Desktop.getDesktop().open(myFile);

    }

}
