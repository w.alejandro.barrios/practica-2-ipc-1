package usac.ipc1.init;

import usac.ipc1.Principal;
import usac.ipc1.beans.Juegos;
import usac.ipc1.beans.Nivel;
import usac.ipc1.beans.Personaje;
import usac.ipc1.views.MainMenu;

import javax.swing.*;

public class Init {

    public Init(){
        Principal.contadorPersonajes = 0;
        Principal.contadorNiveles = 0;
        Principal.juegos = new Juegos[100];
        Principal.contadorJuegos = 0;

        // CONFIG
        Principal.selectRight = "Flecha Der.";
        Principal.selectRightCode = 39;
        Principal.selectLeft = "Flecha Izq.";
        Principal.selectLeftCode = 37;
        Principal.pause = "p";
        Principal.pauseCode = 80;
        Principal.playAgain = "r";
        Principal.playAgainCode = 82;
        Principal.finish = "f";
        Principal.finishCode = 70;

        SwingUtilities.invokeLater(()-> new MainMenu());

    }
}
