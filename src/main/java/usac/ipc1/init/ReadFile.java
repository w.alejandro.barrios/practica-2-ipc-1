package usac.ipc1.init;

import usac.ipc1.Principal;
import usac.ipc1.beans.Nivel;
import usac.ipc1.beans.Personaje;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class ReadFile {

    public ReadFile(File file, Frame frame){
        countLines(file);
        reading(file, frame);
    }

    public void countLines(File file){
        String line = null;
        try{

            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            int cantidadNiveles = 0;
            int cantidadPersonajes = 0;

            while ((line = bufferedReader.readLine()) != null) {
                String[] lineReaded = line.split("\\*\\*");
                String destiny = lineReaded[0];
                switch (destiny){
                    case "PERSONAJE":
                        cantidadPersonajes++;
                        break;

                    case "NIVEL":
                        cantidadNiveles++;
                        break;
                }
            }
            Principal.personajes = new Personaje[cantidadPersonajes];
            Principal.niveles = new Nivel[cantidadNiveles];

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("No se puede abrir el archivo " + file.getName());
            e.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error leyendo archivo " + file.getName());
            ioe.printStackTrace();
        }
    }

    public void reading(File file, Frame frame) {
        String lineFile = null;

        try {

            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReaderFile = new BufferedReader(fileReader);

            while ((lineFile = bufferedReaderFile.readLine()) != null) {
                Point PUNTO_INICIAL = new Point(100, 0);
                String[] lineReadedFile = lineFile.split("\\*\\*");
                String destiny = lineReadedFile[0];
                switch (destiny){
                    case "PERSONAJE":
                        Personaje personaje = new Personaje(lineReadedFile[2], PUNTO_INICIAL.x, PUNTO_INICIAL.y, 25, 25);
                        personaje.setId(Principal.contadorPersonajes);
                        personaje.setNombre(lineReadedFile[1]);
                        personaje.setRutaImagen(lineReadedFile[2]);
                        Principal.personajes[Principal.contadorPersonajes] = personaje;
                        Principal.contadorPersonajes++;
                        break;

                    case "NIVEL":
                        Nivel nivel = new Nivel();
                        nivel.setNivelId(Integer.parseInt(lineReadedFile[1]));
                        nivel.setDistanciaMaxima(Integer.parseInt(lineReadedFile[2]));
                        nivel.setDistanciaPantalla(Integer.parseInt(lineReadedFile[3]));
                        nivel.setVelocidadCorriente(Integer.parseInt(lineReadedFile[4]));
                        nivel.setTiempoNivel(Integer.parseInt(lineReadedFile[5]));
                        nivel.setTiempoSumergir(Integer.parseInt(lineReadedFile[6]));
                        nivel.setPuntosSeleccion(Integer.parseInt(lineReadedFile[7]));
                        nivel.setPuntosSeleccionSumergir(Integer.parseInt(lineReadedFile[8]));
                        Principal.niveles[Principal.contadorNiveles] = nivel;
                        Principal.contadorNiveles++;
                        break;
                }
            }

            bufferedReaderFile.close();
            JOptionPane.showMessageDialog(frame,
                    file.getName() + " cargado exitosamente",
                    "Configuracion",
                    JOptionPane.WARNING_MESSAGE);

        } catch (FileNotFoundException e) {
            System.out.println("No se puede abrir el archivo " + file.getName());
            e.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error leyendo archivo " + file.getName());
            ioe.printStackTrace();
        }

    }

}
