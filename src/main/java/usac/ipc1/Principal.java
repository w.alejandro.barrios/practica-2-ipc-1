package usac.ipc1;

import usac.ipc1.beans.Juegos;
import usac.ipc1.beans.Nivel;
import usac.ipc1.beans.Personaje;

public class Principal {

    public static Personaje[] personajes;
    public static int contadorPersonajes;
    public static Nivel[] niveles;
    public static int contadorNiveles;
    public static Juegos[] juegos;
    public static int contadorJuegos;

    // CONFIG
    public static String selectRight;
    public static int selectRightCode;
    public static String selectLeft;
    public static int selectLeftCode;
    public static String pause;
    public static int pauseCode;
    public static String playAgain;
    public static int playAgainCode;
    public static String finish;
    public static int finishCode;

}
