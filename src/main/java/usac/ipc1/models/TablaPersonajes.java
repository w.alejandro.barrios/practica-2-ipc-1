package usac.ipc1.models;

import usac.ipc1.beans.Personaje;

import javax.swing.table.AbstractTableModel;

public class TablaPersonajes extends AbstractTableModel {

    private Personaje[] vectorPersonaje;
    private final String[] columnName = new String[]{"Id.", "Nombre"};

    public TablaPersonajes(Personaje[] vectorPersonaje) {
        this.vectorPersonaje = vectorPersonaje;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorPersonaje.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Personaje personaje = this.vectorPersonaje[rowIndex];
        if (personaje != null) {
            switch (columnIndex) {
                case 0:
                    value = Integer.toString(personaje.getId());
                    break;

                case 1:
                    value = personaje.getNombre();
                    break;
            }
        }

        return value;
    }

}
