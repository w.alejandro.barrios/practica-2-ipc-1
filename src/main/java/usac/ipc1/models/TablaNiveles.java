package usac.ipc1.models;

import usac.ipc1.beans.Nivel;

import javax.swing.table.AbstractTableModel;

public class TablaNiveles extends AbstractTableModel {

    private Nivel vectorNivel[];
    private final String[] columnName = new String[]{"Nivel", "Distancia Max.", "Distancia Pantalla",
            "Velocidad corriente", "Tiempo", "Tiempo para sumergir", "Puntos", "Puntos al sumergir"};

    public TablaNiveles(Nivel[] vectorNivel) {
        this.vectorNivel = vectorNivel;
        this.fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return this.vectorNivel.length;
    }

    @Override
    public int getColumnCount() {
        return this.columnName.length;
    }

    public String getColumnName(int columnIndex) {
        return this.columnName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String value = "";
        Nivel nivel = this.vectorNivel[rowIndex];
        if (nivel != null) {
            switch (columnIndex) {
                case 0:
                    value = Integer.toString(nivel.getNivelId());
                    break;

                case 1:
                    value = Integer.toString(nivel.getDistanciaMaxima());
                    break;

                case 2:
                    value = Integer.toString(nivel.getDistanciaPantalla());
                    break;

                case 3:
                    value = Integer.toString(nivel.getVelocidadCorriente());
                    break;

                case 4:
                    value = Integer.toString(nivel.getTiempoNivel());
                    break;

                case 5:
                    value = Integer.toString(nivel.getTiempoSumergir());
                    break;

                case 6:
                    value = Integer.toString(nivel.getPuntosSeleccion());
                    break;

                case 7:
                    value = Integer.toString(nivel.getPuntosSeleccionSumergir());
                    break;
            }
        }

        return value;
    }

}
