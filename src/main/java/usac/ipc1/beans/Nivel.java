package usac.ipc1.beans;

public class Nivel {

    private int nivelId;
    private int distanciaMaxima;
    private int distanciaPantalla;
    private int velocidadCorriente;
    private int tiempoNivel;
    private int tiempoSumergir;
    private int puntosSeleccion;
    private int puntosSeleccionSumergir;

    public Nivel(){

    }

    public Nivel(int nivelId, int distanciaMaxima, int distanciaPantalla, int velocidadCorriente, int tiempoNivel, int tiempoSumergir, int puntosSeleccion, int puntosSeleccionSumergir) {
        this.nivelId = nivelId;
        this.distanciaMaxima = distanciaMaxima;
        this.distanciaPantalla = distanciaPantalla;
        this.velocidadCorriente = velocidadCorriente;
        this.tiempoNivel = tiempoNivel;
        this.tiempoSumergir = tiempoSumergir;
        this.puntosSeleccion = puntosSeleccion;
        this.puntosSeleccionSumergir = puntosSeleccionSumergir;
    }

    public int getNivelId() {
        return nivelId;
    }

    public void setNivelId(int nivelId) {
        this.nivelId = nivelId;
    }

    public int getDistanciaMaxima() {
        return distanciaMaxima;
    }

    public void setDistanciaMaxima(int distanciaMaxima) {
        this.distanciaMaxima = distanciaMaxima;
    }

    public int getDistanciaPantalla() {
        return distanciaPantalla;
    }

    public void setDistanciaPantalla(int distanciaPantalla) {
        this.distanciaPantalla = distanciaPantalla;
    }

    public int getVelocidadCorriente() {
        return velocidadCorriente;
    }

    public void setVelocidadCorriente(int velocidadCorriente) {
        this.velocidadCorriente = velocidadCorriente;
    }

    public int getTiempoNivel() {
        return tiempoNivel;
    }

    public void setTiempoNivel(int tiempoNivel) {
        this.tiempoNivel = tiempoNivel;
    }

    public int getTiempoSumergir() {
        return tiempoSumergir;
    }

    public void setTiempoSumergir(int tiempoSumergir) {
        this.tiempoSumergir = tiempoSumergir;
    }

    public int getPuntosSeleccion() {
        return puntosSeleccion;
    }

    public void setPuntosSeleccion(int puntosSeleccion) {
        this.puntosSeleccion = puntosSeleccion;
    }

    public int getPuntosSeleccionSumergir() {
        return puntosSeleccionSumergir;
    }

    public void setPuntosSeleccionSumergir(int puntosSeleccionSumergir) {
        this.puntosSeleccionSumergir = puntosSeleccionSumergir;
    }
}
