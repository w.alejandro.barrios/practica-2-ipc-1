package usac.ipc1.beans;

import javax.swing.*;
import java.awt.*;

public class Personaje extends JLabel {

    private int id;
    private String nombre;
    private String rutaImagen;

    public Personaje(String ruta, int x, int y, int ancho, int alto) {
        super();
        setIcon(new ImageIcon(ruta));
        setBounds(x, y, ancho, alto);
    }

    public Personaje(String ruta, Point ubicacion, int ancho, int alto) {
        super();
        setIcon(new ImageIcon(ruta));
        setBounds(ubicacion.x, ubicacion.y, ancho, alto);
    }

    public void setImagen(String ruta) {
        setIcon(new ImageIcon(ruta));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }
}
