package usac.ipc1.beans;

public class Juegos {

    private int id;
    private int puntosObtenidos;
    private String nombreJugador;

    public Juegos() {
    }

    public Juegos(int id, int puntosObtenidos, String nombreJugador) {
        this.id = id;
        this.puntosObtenidos = puntosObtenidos;
        this.nombreJugador = nombreJugador;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPuntosObtenidos() {
        return puntosObtenidos;
    }

    public void setPuntosObtenidos(int puntosObtenidos) {
        this.puntosObtenidos = puntosObtenidos;
    }

    public String getNombreJugador() {
        return nombreJugador;
    }

    public void setNombreJugador(String nombreJugador) {
        this.nombreJugador = nombreJugador;
    }
}
