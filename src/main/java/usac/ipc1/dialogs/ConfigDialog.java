package usac.ipc1.dialogs;

import usac.ipc1.Principal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ConfigDialog extends JDialog {

    public ConfigDialog(JFrame padre, boolean modal){
        super(padre, modal);
        setBounds(new Rectangle(500, 250));
        setLocationRelativeTo(null);
        setTitle("Configuracion");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        JLabel lblLeft = new JLabel("Izquierda");
        JTextField txtLeft = new JTextField();
        txtLeft.setText(Principal.selectLeft);
        txtLeft.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 8){
                    Principal.selectLeftCode = e.getKeyCode();
                    Principal.selectLeft = Character.toString(e.getKeyChar());
                    txtLeft.setText(Character.toString(e.getKeyChar()));
                    txtLeft.setEnabled(false);
                }

            }
        });

        JLabel lblRight = new JLabel("Derecha");
        JTextField txtRight = new JTextField();
        txtRight.setText(Principal.selectRight);
        txtRight.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 8){
                    Principal.selectRightCode = e.getKeyCode();
                    Principal.selectRight = Character.toString(e.getKeyChar());
                    txtRight.setText(Character.toString(e.getKeyChar()));
                    txtRight.setEnabled(false);
                }

            }
        });

        JLabel lblPause = new JLabel("Pausar");
        JTextField txtPause = new JTextField();
        txtPause.setText(Principal.pause);
        txtPause.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 8){
                    Principal.pauseCode = e.getKeyCode();
                    Principal.pause = Character.toString(e.getKeyChar());
                    txtPause.setText(Character.toString(e.getKeyChar()));
                    txtPause.setEnabled(false);
                }

            }
        });

        JLabel lblReplay = new JLabel("Reanudar");
        JTextField txtReplay = new JTextField();
        txtReplay.setText(Principal.playAgain);
        txtReplay.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 8){
                    Principal.playAgainCode = e.getKeyCode();
                    Principal.playAgain = Character.toString(e.getKeyChar());
                    txtReplay.setText(Character.toString(e.getKeyChar()));
                    txtReplay.setEnabled(false);
                }

            }
        });

        JLabel lblFinish = new JLabel("Finalizar");
        JTextField txtFinish = new JTextField();
        txtFinish.setText(Principal.finish);
        txtFinish.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() != 8){
                    Principal.finishCode = e.getKeyCode();
                    Principal.finish = Character.toString(e.getKeyChar());
                    txtFinish.setText(Character.toString(e.getKeyChar()));
                    txtFinish.setEnabled(false);
                }

            }
        });

        JButton btnVolver = new JButton("Volver");
        btnVolver.addActionListener(e -> {
            dispose();
        });

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500, 50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblLeft);
        contenedor1.add(txtLeft);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500, 50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblRight);
        contenedor2.add(txtRight);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500, 50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblPause);
        contenedor3.add(txtPause);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500, 50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblReplay);
        contenedor4.add(txtReplay);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500, 50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblFinish);
        contenedor5.add(txtFinish);

        JPanel contenedor10 = new JPanel();
        contenedor10.setBackground(Color.white);
        contenedor10.setSize(500, 50);
        BoxLayout boxlayout10 = new BoxLayout(contenedor10, BoxLayout.Y_AXIS);
        contenedor10.setLayout(boxlayout10);
        contenedor10.add(contenedor1);
        contenedor10.add(contenedor2);
        contenedor10.add(contenedor3);
        contenedor10.add(contenedor4);
        contenedor10.add(contenedor5);
        contenedor10.add(btnVolver);

        add(contenedor10);
    }

}
