package usac.ipc1.dialogs;

import usac.ipc1.beans.Nivel;
import usac.ipc1.views.Table;

import javax.swing.*;
import java.awt.*;

public class NivelDialog extends JDialog {

    private Nivel[] vectorNivel;
    private int position;

    public NivelDialog(JFrame padre, boolean modal, Nivel vectorNivel[], Table table, int position){
        super(padre, modal);
        setBounds(new Rectangle(500,250));
        setLocationRelativeTo(null);
        setTitle("Editar Nivel");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorNivel = vectorNivel;
        this.position = position;

        buildDialog(table);
    }

    private void buildDialog(Table table){
        JLabel lblNivel = new JLabel("Nivel");
        JTextField txtNivel = new JTextField();
        txtNivel.setEnabled(false);

        JLabel lblDMax = new JLabel("Distancia maxima");
        JTextField txtDMax = new JTextField();

        JLabel lblDPantalla = new JLabel("Distancia en pantalla");
        JTextField txtDPantalla = new JTextField();

        JLabel lblVelocidad = new JLabel("Velocidad corriente");
        JTextField txtVelocidad = new JTextField();

        JLabel lblTiempo = new JLabel("Tiempo nivel");
        JTextField txtTiempo = new JTextField();

        JLabel lblTSumergir = new JLabel("Tiempo sumergir");
        JTextField txtTSumergir = new JTextField();

        JLabel lblPSeleccion = new JLabel("Puntos seleccion");
        JTextField txtPSeleccion = new JTextField();

        JLabel lblPSeleccionSumergir = new JLabel("Puntos seleccion sumergir");
        JTextField txtPSeleccionSumergir = new JTextField();

        txtNivel.setText(Integer.toString(vectorNivel[position].getNivelId()));
        txtDMax.setText(Integer.toString(vectorNivel[position].getDistanciaMaxima()));
        txtDPantalla.setText(Integer.toString(vectorNivel[position].getDistanciaPantalla()));
        txtVelocidad.setText(Integer.toString(vectorNivel[position].getVelocidadCorriente()));
        txtTiempo.setText(Integer.toString(vectorNivel[position].getTiempoNivel()));
        txtTSumergir.setText(Integer.toString(vectorNivel[position].getTiempoSumergir()));
        txtPSeleccion.setText(Integer.toString(vectorNivel[position].getPuntosSeleccion()));
        txtPSeleccionSumergir.setText(Integer.toString(vectorNivel[position].getPuntosSeleccionSumergir()));

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            vectorNivel[position].setDistanciaMaxima(Integer.parseInt(txtDMax.getText()));
            vectorNivel[position].setDistanciaPantalla(Integer.parseInt(txtDPantalla.getText()));
            vectorNivel[position].setVelocidadCorriente(Integer.parseInt(txtVelocidad.getText()));
            vectorNivel[position].setTiempoNivel(Integer.parseInt(txtTiempo.getText()));
            vectorNivel[position].setTiempoSumergir(Integer.parseInt(txtTSumergir.getText()));
            vectorNivel[position].setPuntosSeleccion(Integer.parseInt(txtPSeleccion.getText()));
            vectorNivel[position].setPuntosSeleccionSumergir(Integer.parseInt(txtPSeleccionSumergir.getText()));
            table.editar(vectorNivel[position], position);
            JOptionPane.showMessageDialog(NivelDialog.this, "Cambios efectuados exitosamente");
            dispose();
        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500,50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblNivel);
        contenedor1.add(txtNivel);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500,50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblDMax);
        contenedor2.add(txtDMax);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500,50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblDPantalla);
        contenedor3.add(txtDPantalla);

        // Contenedor 4
        JPanel contenedor4 = new JPanel();
        contenedor4.setBackground(Color.WHITE);
        contenedor4.setSize(500,50);
        BoxLayout boxLayout4 = new BoxLayout(contenedor4, BoxLayout.X_AXIS);
        contenedor4.setLayout(boxLayout4);
        contenedor4.add(lblVelocidad);
        contenedor4.add(txtVelocidad);

        // Contenedor 5
        JPanel contenedor5 = new JPanel();
        contenedor5.setBackground(Color.WHITE);
        contenedor5.setSize(500,50);
        BoxLayout boxLayout5 = new BoxLayout(contenedor5, BoxLayout.X_AXIS);
        contenedor5.setLayout(boxLayout5);
        contenedor5.add(lblTiempo);
        contenedor5.add(txtTiempo);

        // Contenedor 6
        JPanel contenedor6 = new JPanel();
        contenedor6.setBackground(Color.WHITE);
        contenedor6.setSize(500,50);
        BoxLayout boxLayout6 = new BoxLayout(contenedor6, BoxLayout.X_AXIS);
        contenedor6.setLayout(boxLayout6);
        contenedor6.add(lblTSumergir);
        contenedor6.add(txtTSumergir);

        // Contenedor 7
        JPanel contenedor7 = new JPanel();
        contenedor7.setBackground(Color.WHITE);
        contenedor7.setSize(500,50);
        BoxLayout boxLayout7 = new BoxLayout(contenedor7, BoxLayout.X_AXIS);
        contenedor7.setLayout(boxLayout7);
        contenedor7.add(lblPSeleccion);
        contenedor7.add(txtPSeleccion);

        // Contenedor 8
        JPanel contenedor8 = new JPanel();
        contenedor8.setBackground(Color.WHITE);
        contenedor8.setSize(500,50);
        BoxLayout boxLayout8 = new BoxLayout(contenedor8, BoxLayout.X_AXIS);
        contenedor8.setLayout(boxLayout8);
        contenedor8.add(lblPSeleccionSumergir);
        contenedor8.add(txtPSeleccionSumergir);

        // Contenedor 9
        JPanel contenedor9 = new JPanel();
        contenedor9.setBackground(Color.WHITE);
        contenedor9.setSize(500,50);
        BoxLayout boxLayout9 = new BoxLayout(contenedor9, BoxLayout.X_AXIS);
        contenedor9.setLayout(boxLayout9);
        contenedor9.add(btnSave);
        contenedor9.add(btnCancel);

        JPanel contenedor10 = new JPanel();
        contenedor10.setBackground(Color.white);
        contenedor10.setSize(500, 50);
        BoxLayout boxlayout10 = new BoxLayout(contenedor10, BoxLayout.Y_AXIS);
        contenedor10.setLayout(boxlayout10);
        contenedor10.add(contenedor1);
        contenedor10.add(contenedor2);
        contenedor10.add(contenedor3);
        contenedor10.add(contenedor4);
        contenedor10.add(contenedor5);
        contenedor10.add(contenedor6);
        contenedor10.add(contenedor7);
        contenedor10.add(contenedor8);
        contenedor10.add(contenedor9);

        add(contenedor10);
    }

}
