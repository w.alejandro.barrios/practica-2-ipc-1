package usac.ipc1.dialogs;

import usac.ipc1.Principal;
import usac.ipc1.beans.Personaje;
import usac.ipc1.views.Table;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;

public class PersonajeDialog extends JDialog {

    private Personaje[] vectorPersonaje;
    private int position;

    public PersonajeDialog(JFrame padre, boolean modal, Personaje vectorPersonaje[], Table table, int position) {
        super(padre, modal);
        setBounds(new Rectangle(500, 200));
        setLocationRelativeTo(null);
        setTitle("Editar Personaje");
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        this.vectorPersonaje = vectorPersonaje;
        this.position = position;

        buildDialog(table);
    }

    private void buildDialog(Table table) {
        JLabel lblId = new JLabel("Id");
        JTextField txtId = new JTextField();
        txtId.setEnabled(false);

        JLabel lblNombre = new JLabel("Nombre personaje");
        JTextField txtNombre = new JTextField();

        JLabel lblImage = new JLabel();
        lblImage.setPreferredSize(new Dimension(50,50));
        JLabel lblRuta = new JLabel("Ruta imagen");
        JTextField txtRuta = new JTextField();
        txtRuta.setEnabled(false);

        txtId.setText(Integer.toString(vectorPersonaje[position].getId()));
        txtNombre.setText(vectorPersonaje[position].getNombre());
        txtRuta.setText(vectorPersonaje[position].getRutaImagen());
        lblImage.setIcon(new ImageIcon(vectorPersonaje[position].getRutaImagen()));

        JButton btnChangeImage = new JButton("Nueva imagen");
        btnChangeImage.addActionListener(e -> {
            JFileChooser jFileChooser = new JFileChooser("../Desktop/");
            jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

            FileFilter filtro = new FileNameExtensionFilter("Imagenes (*.png, *.jpg, *.jpeg)", "png", "jpg", "jpeg");
            jFileChooser.setFileFilter(filtro);
            if (jFileChooser.showDialog(PersonajeDialog.this, "Seleccionar imagen") == JFileChooser.APPROVE_OPTION){
                boolean existe = false;
                for (int i = 0; i < Principal.personajes.length; i++) {
                    if (Principal.personajes[i] != null){
                        if (i != position){
                            if (Principal.personajes[i].getRutaImagen().equals(jFileChooser.getSelectedFile().getAbsolutePath()))
                                existe = true;
                        }
                    }
                }
                if (!existe){
                    txtRuta.setText(jFileChooser.getSelectedFile().getAbsolutePath());
                    lblImage.setIcon(new ImageIcon(jFileChooser.getSelectedFile().getAbsolutePath()));
                }else{
                    JOptionPane.showMessageDialog(PersonajeDialog.this, "Esta imagen ya esta siendo utilizada");
                }

            }
        });

        JButton btnSave = new JButton("Guardar");
        btnSave.addActionListener(e -> {
            boolean existe = false;
            for (int i = 0; i < Principal.personajes.length; i++) {
                if (Principal.personajes[i] != null){
                    if (i != position){
                        if (Principal.personajes[i].getNombre().equals(txtNombre.getText()))
                            existe = true;
                    }
                }
            }
            if (!existe){
                vectorPersonaje[position].setNombre(txtNombre.getText());
                vectorPersonaje[position].setRutaImagen(txtRuta.getText());
                vectorPersonaje[position].setImagen(txtRuta.getText());
                table.editar(vectorPersonaje[position], position);
                JOptionPane.showMessageDialog(PersonajeDialog.this, "Cambios efectuados exitosamente");
                dispose();
            }else{
                JOptionPane.showMessageDialog(PersonajeDialog.this, "Este nombre ya esta tomado");
            }

        });

        JButton btnCancel = new JButton("Cancelar");
        btnCancel.addActionListener(e -> dispose());

        // Contenedor 1
        JPanel contenedor1 = new JPanel();
        contenedor1.setBackground(Color.WHITE);
        contenedor1.setSize(500, 50);
        BoxLayout boxLayout1 = new BoxLayout(contenedor1, BoxLayout.X_AXIS);
        contenedor1.setLayout(boxLayout1);
        contenedor1.add(lblId);
        contenedor1.add(txtId);

        // Contenedor 2
        JPanel contenedor2 = new JPanel();
        contenedor2.setBackground(Color.WHITE);
        contenedor2.setSize(500, 50);
        BoxLayout boxLayout2 = new BoxLayout(contenedor2, BoxLayout.X_AXIS);
        contenedor2.setLayout(boxLayout2);
        contenedor2.add(lblNombre);
        contenedor2.add(txtNombre);

        // Contenedor 3
        JPanel contenedor3 = new JPanel();
        contenedor3.setBackground(Color.WHITE);
        contenedor3.setSize(500, 50);
        BoxLayout boxLayout3 = new BoxLayout(contenedor3, BoxLayout.X_AXIS);
        contenedor3.setLayout(boxLayout3);
        contenedor3.add(lblRuta);
        contenedor3.add(txtRuta);
        contenedor3.add(btnChangeImage);

        // Contenedor 9
        JPanel contenedor9 = new JPanel();
        contenedor9.setBackground(Color.WHITE);
        contenedor9.setSize(500, 50);
        BoxLayout boxLayout9 = new BoxLayout(contenedor9, BoxLayout.X_AXIS);
        contenedor9.setLayout(boxLayout9);
        contenedor9.add(btnSave);
        contenedor9.add(btnCancel);

        JPanel contenedor10 = new JPanel();
        contenedor10.setBackground(Color.white);
        contenedor10.setSize(500, 50);
        BoxLayout boxlayout10 = new BoxLayout(contenedor10, BoxLayout.Y_AXIS);
        contenedor10.setLayout(boxlayout10);
        contenedor10.add(contenedor1);
        contenedor10.add(contenedor2);
        contenedor10.add(lblImage);
        contenedor10.add(contenedor3);
        contenedor10.add(contenedor9);

        add(contenedor10);
    }

}
